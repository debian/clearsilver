clearsilver (0.10.5-6) unstable; urgency=medium

  * QA upload.

  * Trim trailing whitespace.
  * Flagged in d/control that no root is needed during build.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 06 May 2024 21:33:41 +0200

clearsilver (0.10.5-5) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.1.0 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Moved to debhelper compat level 12.
  * Fixed typo in package description (deverlopers->developers)
    (Closes: #923189).

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 28 Apr 2024 10:16:22 +0200

clearsilver (0.10.5-4) unstable; urgency=medium

  * QA upload.
  * Drop the Python subpackage.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 21 Aug 2019 22:07:40 +0500

clearsilver (0.10.5-3) unstable; urgency=medium

  * QA upload.
  * Run "make distclean" only if rules.mk exists; fixes FTBFS on arm64 mips*
    ppc64el.  Closes: #873589

 -- Adam Borowski <kilobyte@angband.pl>  Thu, 31 Aug 2017 01:54:40 +0200

clearsilver (0.10.5-2) unstable; urgency=medium

  * QA upload.
  * Orphan the package.  See #849019; Closes: #834987
  * Run wrap-and-sort to sort all debian/* files.
  * Drop explic usage of quilt, unneeded for a 3.0 (quilt) source package.
  * Rewrite Debian build system to use the dh sequencer instead of cdbs.
  * Bump debhelper compat level to 10.
  * debian/README.source:
    + remove, nowadays everybody recognizes a quilt-managed package.
  * d/clean:
    + Add file to do a more thorough clean and make possible to build the
      package twice in a row.  Closes: #621779
  * debian/control:
    + Bump Standards-Version to 4.1.0, no changes needed.
    + Move clearsilver-dev from section python to section libdevel
      Closes: #832194
    + Drop obsolete package relationship long satisfied everywhere.
    + Drop long obsoleted XS-Python-Version:all
    + Add ${shlibs:Depends} to clearsilver-dev, this end up adding a dependency
      on zlib1g and libc6, as it should have been since the start.
    + De-duplicate the long descriptions.
    + De-duplicate Priority definition.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 26 Aug 2017 22:20:14 +0200

clearsilver (0.10.5-1.6) unstable; urgency=medium

  * Non-maintainer upload.
  * Port to dh_python2 from python-support.  Closes: #786096
    + debian/control: switch build-dependency
    + debian/control: remove XB-Python-Version field
    + debian/pycompat: remove
    + debian/rules: remove DEB_PYTHON_SYSTEM

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 12 Dec 2015 17:45:06 +0000

clearsilver (0.10.5-1.5) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS with clang instead of gcc" and "ftbfs with gcc5":
    apply patch from Alexander <sanek23994@gmail.com>
    (Closes: #755963)

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Jul 2015 18:11:32 +0200

clearsilver (0.10.5-1.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "hardcodes /usr/lib/perl5":
    - Make libclearsilver-perl.install executable and use $Config{vendorarch}.
    - Use $Config{vendorarch} also in debian/rules.
    - Bump debhelper compat level and dependency to 9.
    (Closes: #752469)

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Aug 2014 17:51:13 +0200

clearsilver (0.10.5-1.3) unstable; urgency=high

  * Non-maintainer upload.
  * Fix format string vulnerability CVE-2011-4357 (Closes: #649322).

 -- Luk Claes <luk@debian.org>  Thu, 29 Dec 2011 21:57:11 +0100

clearsilver (0.10.5-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Drop wrong debian-changes-0.10.5-1 patch (Closes: #643474).

 -- Didier Raboud <odyx@debian.org>  Tue, 15 Nov 2011 16:24:49 +0100

clearsilver (0.10.5-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use python-support (closes: #620271).
    + Add build-dependency on python-support.
    + In debian/rules:
      - set DEB_PYTHON_SYSTEM to pysupport;
      - include class/python-module.mk.

 -- Jakub Wilk <jwilk@debian.org>  Fri, 08 Apr 2011 21:49:37 +0200

clearsilver (0.10.5-1) unstable; urgency=low

  * New upstream release (Closes: #497008)
  * Fix manpage format errors
  * Remove obsolete patch for document.py (upstream fixed it)
  * fix FTBFS bug by removing DEB_MAKE_CHECK_TARGET (Closes: #577320)
  * remove obsolete "clean" target for "autotools"
    (update config.{guess|sub})
  * Patched failing CRC test for 64bit systems
    * see: http://tech.groups.yahoo.com/group/ClearSilver/message/1178
  * Update to Standards-Version 3.8.4:
    * add "Homepage" field to control file
    * remove versioned dependency on on perl >= 5.6
    * switche to new source package format "3.0 (quilt)"
  * Deny orphaning request (Closes: #543735)

 -- Lars Kruse <devel@sumpfralle.de>  Thu, 29 Apr 2010 03:52:15 +0200

clearsilver (0.10.4-1.4) unstable; urgency=low

  * Non-maintainer upload.
  * Python 2.6 transition (Closes: #557794):
    + debian/control:
      - Build-depend on python-all-dev (>= 2.5.4-1~).
    + debian/rules:
      - Include /usr/share/python/python.mk.
      - Use py_sitename to determine correcy installation directory.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 08 Dec 2009 19:13:14 +0100

clearsilver (0.10.4-1.3) unstable; urgency=low

  * Non-maintainer upload.
  * debian/rules:
    + Fix bashism (Closes: #477253).

 -- Marco Rodrigues <gothicx@sapo.pt>  Mon, 16 Jun 2008 12:19:37 +0100

clearsilver (0.10.4-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/rules: Build python bindings for each python version (instead
    of using the same binary for all python versions).
    Patch by Chris Lamb. (Closes: #452612)

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Thu, 24 Apr 2008 11:11:37 +0200

clearsilver (0.10.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add missing substvars in debian/control (Closes: 463094).
  * Bump Standards-Version to 3.7.3.

 -- Pierre Habouzit <madcoder@debian.org>  Tue, 01 Apr 2008 20:55:25 +0000

clearsilver (0.10.4-1) unstable; urgency=low

  * New upstream release

 -- Lars Kruse <devel@sumpfralle.de>  Sat, 16 Jun 2007 22:46:26 +0200

clearsilver (0.10.3-4.1) unstable; urgency=high

  * Non-maintainer upload.
  * Add conflict to libcgi-dev to clearsilver-dev because of filename
    conflict. Closes: #403996

 -- Andreas Barth <aba@not.so.argh.org>  Tue, 26 Dec 2006 11:04:03 +0000

clearsilver (0.10.3-4) unstable; urgency=low

  * Renamed perl-clearsilver to libclearsilver-perl according to debian
    policy (Closes: #384158)

 -- Lars Kruse <devel@sumpfralle.de>  Wed, 23 Aug 2006 14:29:26 +0200

clearsilver (0.10.3-3) unstable; urgency=low

  * Updated config.guess and config.sub for mips/mipsel (Closes: #378595)
  * Improved makefile to remove root-owned files after "make clean"

 -- Lars Kruse <devel@sumpfralle.de>  Sat, 29 Jul 2006 16:48:43 +0200

clearsilver (0.10.3-2) unstable; urgency=low

  [ Otavio Salvador ]
  * Move package maintainence to Collaborative Maintainence Project.
  * Disable C# building since it's broken in current release with current
    MCS compiler.
  * Update Standards-Version to 3.7.2 (no changes)

  [ Lars Kruse ]
  * perl-clearsilver: wrong installation directory in original makefile
    and broken debian/rules fixed (Closes: #357908)
  * Add header files, manpages and libraries to the clearsilver-dev package
    (Closes: #293888, #346438)
  * Fix dependency of clearsilver-dev (Closes: #372759)
  * Replace python2.3-clearsilver package with python-clearsilver package for
    python 2.3 and 2.4 (Closes: #370669, #337838)
  * Switch to new python policy (Closes: #373314)

  [ Jesus Climent ]
  * Upload to unstable.

 -- Jesus Climent <jesus.climent@hispalinux.es>  Sun, 25 Jun 2006 00:05:36 +0300

clearsilver (0.10.3-1) unstable; urgency=low

  * New upstream release
  * Ack previous NMUs (Closes: #310073, #310231);
  * Leave CDBS set the debhelper compatibility level;
  * Add watch file to be easier to update it next time;
  * Add zlib1g-dev as build-depends (Closes: #357278);
  * Simplified python-clearsilver dependencies;
  * Use ${python:Depends} in python2.3-clearsilver;
  * Update Standards-Version. No changes needed;
  * Include autotools-dev since it uses autoconf and it makes this package
    more portable;

 -- Otavio Salvador <otavio@debian.org>  Sat,  8 Apr 2006 07:51:10 -0300

clearsilver (0.10.2-1) unstable; urgency=low

  * New upstream release.
  * Added perl bindings.

 -- Jesus Climent <jesus.climent@hispalinux.es>  Sun,  8 Jan 2006 15:45:00 +0000

clearsilver (0.9.14-1) unstable; urgency=low

  * New upstream release

 -- Jesus Climent <jesus.climent@hispalinux.es>  Wed, 15 Jun 2005 08:35:36 +0000

clearsilver (0.9.13-2) unstable; urgency=medium

  * Correct clearsilver-dev dependency on clearsilver1 (Closes: #293574)

 -- Jesus Climent <jesus.climent@hispalinux.es>  Fri,  4 Feb 2005 14:04:38 +0000

clearsilver (0.9.13-1) unstable; urgency=low

  * Welcome Otavio as a co-maintainer.
  * This release should clean the RC bugs we have (Closes: #269555), allowing
    PIC in shared libs.

 -- Jesus Climent <jesus.climent@hispalinux.es>  Sun, 16 Jan 2005 16:59:45 +0200

clearsilver (0.9.13-0+1) unstable; urgency=low

  * New upstream release
  * Add myself as co-maintainer
  * Remove capital letter from description synopsis

 -- Otavio Salvador <otavio@debian.org>  Wed,  5 Jan 2005 20:20:50 -0200

clearsilver (0.9.8-3) unstable; urgency=low

  * debian/control: added build-dependency on python (Closes: #268649)
  * debian/rules: added -fPIC for building. Should help to build in hppa and
    amd64. Supposedly (Closes: #273656).

 -- Jesus Climent <jesus.climent@hispalinux.es>  Mon, 27 Sep 2004 20:52:32 +0000

clearsilver (0.9.8-2) unstable; urgency=low

  * debian/control: s/depends/Depends/ (Closes: #268649)

 -- Jesus Climent <jesus.climent@hispalinux.es>  Sat, 28 Aug 2004 17:47:06 +0300

clearsilver (0.9.8-1) unstable; urgency=low

  * First upload.

 -- Jesus Climent <jesus.climent@hispalinux.es>  Mon,  2 Aug 2004 11:27:43 +0000
